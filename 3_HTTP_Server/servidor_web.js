// Archivo: servidor_practica3.js

'use strict';

const net = require('net');
const path = require('path');
const fs = require('fs');

const EOL = '\r\n';
const textoPagina = 'Hello World!' + EOL;
const root = 'root';

function contentType(ext) {
  if (ext === '.html') return 'text/html; charset=utf-8';
  if (ext === '.css') return 'text/css; charset=utf-8';
  if (ext === '.png') return 'image/png';
  return 'application/octet-stream';
}

function creaRespuesta(cuerpo, ext) {
  return Buffer.concat([
    new Buffer(
      'HTTP/1.1 200 OK' + EOL +
      'Server: ServidorPractica/3' + EOL +
      'Date: ' + new Date().toUTCString() + EOL +
      'Content-Length: ' + cuerpo.length + EOL +
      'Content-Type: ' + contentType(ext) + EOL +
      EOL),
    cuerpo]);
}

const servidor = net.createServer(function (sock) {
  sock.on('data', function (data) {
    let texto = data.toString();
    let lineas = texto.split(EOL);
    let peticion = lineas[0];
    let partesPeticion = peticion.split(' ');
    let metodo = partesPeticion[0];
    let recurso = partesPeticion[1];
    let versionProtocolo = partesPeticion[2];
    let ext = path.extname(recurso);
    let dir = path.join(root, recurso);
    
    console.log(dir);
    
    fs.readFile(dir, (err, data) => {
      if (err) {
        console.log(err);
      }
      sock.write(creaRespuesta(data, ext));
      sock.end();
    });
  });
});

servidor.listen(process.env.PORT, function () {
  console.log('Servidor web corriendo en puerto: ' + process.env.PORT);
});
