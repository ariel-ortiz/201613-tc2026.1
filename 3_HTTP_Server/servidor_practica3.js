// Archivo: servidor_practica3.js

'use strict';

const net = require('net');
const EOL = '\r\n';
const textoPagina = 'Hello World!' + EOL;

function creaRespuesta(cuerpo) {
  let bufferCuerpo = new Buffer(cuerpo);
  return Buffer.concat([
    new Buffer(
      'HTTP/1.1 200 OK' + EOL +
      'Server: ServidorPractica/3' + EOL +
      'Date: ' + new Date().toUTCString() + EOL +
      'Content-Length: ' + bufferCuerpo.length + EOL +
      'Content-Type: text/plain; charset=utf-8' + EOL +
      EOL),
    bufferCuerpo]);
}

const servidor = net.createServer(function (sock) {
  sock.on('data', function (data) {
    console.log(data.toString());
    sock.write(creaRespuesta(textoPagina));
    sock.end();
  });
});

servidor.listen(process.env.PORT, function () {
  console.log('Servidor web corriendo en puerto: ' + process.env.PORT);
});
