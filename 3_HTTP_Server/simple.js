// Servidor HTTP muy sencillo.

'use strict';

const net = require('net');
const eol = '\r\n';

let response =
  'HTTP/1.1 200 OK' + eol +
  'Content-Type: text/html; charset=utf-8' + eol +
  eol +
  '<h1>Hola</h1><p>¿Cómo estas?</p>' + eol;

const servidor = net.createServer((socket) => {

  socket.on('data', (data) => {
    let info = data.toString();
    console.log(info);
    socket.write(response);
    socket.end();
  });

});

servidor.listen(process.env.PORT, () => {
  console.log('Servidor HTTP escuchando...');
});