//---------------------------------------------------------
// Coloca aquí tu matrícula y nombre.
//---------------------------------------------------------

'use strict';

const net = require('net');

function cambiaVocalesPorA(cadena) {
  let resultado = [];
  for (let i = 0; i < cadena.length; i++) {
    let c = cadena[i];
    if (c === 'e' || c === 'i' || c === 'o' || c === 'u') {
      resultado.push('a');
    } else if (c === 'E' || c === 'I' || c === 'O' || c === 'U') {
      resultado.push('A');
    } else {
      resultado.push(c);
    }
  }
  return resultado.join('');
}

const servidor = net.createServer((socket) => {
  socket.on('data', (data) => {
    let cadena = data.toString();
    if (cadena.trim() === 'fin') {
      socket.end('Bye\n');
    } else {
      socket.write(cambiaVocalesPorA(cadena));
    }
  });
});

servidor.listen(process.env.PORT, () => {
  console.log('Servidor escuchando...');
});

