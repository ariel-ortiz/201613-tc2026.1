//---------------------------------------------------------
// Coloca aquí tu matrícula y nombre.
//---------------------------------------------------------

'use strict';

const net = require('net');
const path = require('path');
const fs = require('fs');

const EOL = '\r\n';
const root = 'root';
const contentType = new Map([
  ['.html', 'text/html; charset=utf-8'],
  ['.css', 'text/css; charset=utf-8'],
  ['.png', 'image/png'],
  ['.json', 'application/json']
]);

function creaRespuesta(estado, descripcion, cuerpo, ext) {
  cuerpo = cuerpo instanceof Buffer ? cuerpo: new Buffer(cuerpo);
  return Buffer.concat([
    new Buffer(
      'HTTP/1.1 ' + estado + ' ' + descripcion + EOL +
      'Server: ChiquiServidor/0.1' + EOL +
      'Date: ' + new Date().toUTCString() + EOL +
      'Content-Length: ' + cuerpo.length + EOL +
      'Content-Type: ' +
        (contentType.get(ext) || 'application/octet-stream') + EOL +
      EOL),
    cuerpo]);
}

function creaPaginaError(estado, descripcion, mensaje) {
  let pagina =
    '<!DOCTYPE html>' + EOL +
    '<html>' + EOL +
    '  <head><title>' + descripcion + '</title></head>' + EOL +
    '  <body><h1>' + estado + ': ' + descripcion + '</h1>' + EOL +
    '  <p>' + mensaje + '</p></body>' + EOL +
    '</html>' + EOL;
  return creaRespuesta(estado, descripcion, pagina, '.html')
}

const servidor = net.createServer(function (sock) {
  sock.on('data', function (data) {
    let texto = data.toString();
    let lineas = texto.split(EOL);
    let peticion = lineas[0];
    let partesPeticion = peticion.split(' ');
    let metodo = partesPeticion[0];
    let recurso = partesPeticion[1];
    
    if (recurso === '/') {
      recurso = '/index.html';
    }
    
    let ext = path.extname(recurso);
    let dir = path.join(root, recurso);

    console.log(metodo + ' ' + recurso);
    
    if (metodo === 'GET') {
      fs.readFile(dir, (err, data) => {
        if (err) {
          sock.end(creaPaginaError(404, 'Not Found',
            'No se encontró el recurso: ' + recurso));
        } else {
          sock.end(creaRespuesta(200, 'OK', data, ext));
        }
      });
    } else {
      sock.end(creaPaginaError(405, 'Method Not Allowed',
        'Método no soportado: ' + metodo));
    }
  });
});

let host = process.env.C9_HOSTNAME || 'localhost';
let puerto = process.env.PORT || '8080';

servidor.listen(puerto, function () {
  console.log('Nombre de host: ' + host);
  console.log('Puerto: ' + puerto);
});
