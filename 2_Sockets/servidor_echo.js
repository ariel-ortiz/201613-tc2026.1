// Ejemplo de un servidor sencillo que usa sockets TCP.

'use strict';

const net = require('net');

let id = 0;

const servidor = net.createServer((socket) => {
  
  let myId = id;
  id++;
  
  console.log('Se contectó un nuevo cliente ' + myId);
  
  socket.on('data', (data) => {
    let info = data.toString().trim();
    console.log(myId + ': ' + info);
    if (info === 'bye') {
      socket.end();
    } else {
      socket.write(data);
    }
  });
  
  socket.on('end', () => {
    console.log('El cliente se desconectó ' + myId);
  });
});

servidor.listen(process.env.PORT, () => {
  console.log('Servidor escuchando...');
});

