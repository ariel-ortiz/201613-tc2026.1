// Ejemplo de un servidor "broadcast" que usa sockets TCP.

'use strict';

const net = require('net');

let listaSockets = [];

const servidor = net.createServer((socket) => {

  listaSockets.push(socket);

  console.log('Se contectó un nuevo cliente');

  socket.on('data', (data) => {
    let info = data.toString().trim();
    if (info === 'bye') {
      socket.end();
    } else {
      listaSockets.forEach((s) => {
        if (s !== socket) {
          s.write(data);
        }
      });
    }
  });

  socket.on('end', () => {
    let idx = listaSockets.findIndex((x) => {
      return x === socket;
    });
    listaSockets.splice(idx, 1);
    console.log('Un cliente se desconectó');
  });
});

servidor.listen(process.env.PORT, () => {
  console.log('Servidor escuchando...');
});
