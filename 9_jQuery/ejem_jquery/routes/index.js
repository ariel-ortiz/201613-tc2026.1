'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

let frases = [
  'Perro que ladra, no muerde.',
  'Más vale pájaro en mano, que un ciento volando.',
  'Camarón que se duerme, se lo lleva la corriente.',
  'El que madruga, Dios lo ayuda.',
  'Árbol que nace que nace torcido, jamás su tronco endereza.'
];

router.get('/fortuna', (req, res) => {
  let f = (Math.random() * frases.length) | 0;
  let r = { texto: frases[f] };
  res.json(r);
});

module.exports = router;
