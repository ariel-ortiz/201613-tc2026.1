'use strict';

var express = require('express');
var router = express.Router();

router.get('/', (req, res) => {
  res.render('prueba', { titulo: 'Una pruebita' });
});

router.get('/formulario', (req, res) => {
  res.render('formulario', { titulo: 'Formulario'});
});



router.post('/procesa', (req, res) => {
  let nombre = req.body.nombre || 'Sepa';
  let color = req.body.color || 'Desconocido';
  req.session['nombre'] = nombre;
  req.session['color'] = color;
  res.redirect('/continuar');
});

router.get('/continuar', (req, res) => {
  let nombre = req.session['nombre'];
  let color = req.session['color'];
  res.render('procesado', { nombre, 
                            color, 
                            titulo: 'Procesado' });
});

module.exports = router;