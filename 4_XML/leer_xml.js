'use strict';

const fs = require('fs');
const xml2js = require('xml2js');

let parser = new xml2js.Parser();

fs.readFile('movies.xml', function(err, data) {
  if (err) throw err;
  parser.parseString(data, function (err, result) {
    if (err) throw err;      
    console.log(result.movies.film[0].cast[1]);
  });
});