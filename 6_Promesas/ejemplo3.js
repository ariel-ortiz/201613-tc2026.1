// Lee dos archivos y con su contenido genera un tercero.
// Ejemplo usando promesas.

'use strict';

const promisify = require('promisify-node');
const fs = promisify('fs');

fs.readFile('uno.txt').then(data => {
  return fs.writeFile('tres.txt', data);
}).then(() => {
  return fs.readFile('dos.txt');
}).then(data => {
  return fs.appendFile('tres.txt', data);
}).then(() => {
  console.log('Listo!');
}).catch(err => {
  console.log('Error:', err);
})