// Creando una promesa explícitamente.

'use strict';

let p = new Promise((resolve, reject) => {
  setTimeout(resolve, 1000, 'success!');
  setTimeout(reject, 200, 'oops!');
});

p.then(x => {
  console.log(x);
}).catch(err => {
  console.log('Error', err);
});