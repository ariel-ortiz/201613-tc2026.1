// Lee dos archivos y con su contenido genera un tercero.
// Ejemplo usando programación asíncrona.

'use strict';

const fs = require('fs');

fs.readFile('uno.txt', (err, data) => {
  if (err) throw err;
  fs.writeFile('tres.txt', data, (err) => {
    if (err) throw err;
    fs.readFile('dos.txt', (err, data) => {
      if (err) throw err;
      fs.appendFile('tres.txt', data, (err) => {
        if (err) throw err;
        console.log('Listo!');
      });
    });
  });
});