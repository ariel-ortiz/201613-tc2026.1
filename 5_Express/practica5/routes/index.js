'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;

router.get('/hola/:nombre?', function (req, res) {
  let nombre = req.params.nombre || 'Mundo';
  let enanos = ['Tontín', 'Doc', 'Feliz', 'Estornudón',
                'Gruñón', 'Tímido', 'Dormilón'];
  res.render('hola.ejs', { 'nombre': nombre,
                           'num': 42,
                           'enanos': enanos
  });
});
