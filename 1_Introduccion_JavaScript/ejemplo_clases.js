// Ejemplo de uso de clases en ECMAScript 6.

'use strict';

class Alumno {
  constructor (matricula, nombre) {
    this._matricula = matricula;
    this._nombre = nombre;
  }
  
  toString() {
    return 'Hola, me llamo ' + this._nombre + ' (' + this._matricula + ')';
  }
  
  get matricula() {
    return this._matricula;
  }
  
  get nombre() {
    return this._nombre;
  }
  
  set nombre(valor) {
    this._nombre = valor;
  }
}

let a = new Alumno(123, 'Juan');
console.log(a + '');
console.log(a.matricula);
a.nombre = 'Pedro';
console.log(a.nombre);
