// Usando funciones en JavaScript.

'use strict';

function fact(n) {
  let r = 1;
  
  for (let i = 2; i <= n; i++) {
    r *= i;
  }
  return r;
} 

console.log(fact(5));

function f() {
  console.log('Función f');
}

setTimeout(f, 1000);
console.log('Fin');
setTimeout(() => { console.log('Función anónima'); }, 500);
