// Muestra como leer un archivo de manera asíncrona.

'use strict';

const fs = require('fs');

let texto;

fs.readFile('leer_archivo.js', (err, data) => {
  if (err) throw err;
  texto = data.toString();
  console.log(texto);
});

console.log('Fin de programa');
