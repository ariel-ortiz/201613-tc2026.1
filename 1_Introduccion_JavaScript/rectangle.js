// Define la clase rectángulo.

'use strict';

class Rectangle {

  constructor(width, height) {
    this._width = width;
    this._height = height;
  }

  get width() {
    return this._width;
  }

  get height() {
    return this._height;
  }

  perimeter() {
    return (this.width + this.height) * 2;
  }

  area() {
    return this.width * this.height;
  }

  draw() {
    let row = new Array(this.width).fill('*').join(' ');
    let rect = new Array(this.height).fill(row).join('\n');
    console.log(rect);
    console.log();
  }
}

var a = new Rectangle(4, 3);
var b = new Rectangle(5, 10);

console.log("Primer rectángulo");
console.log("Ancho:", a.width);
console.log("Altura:", a.height);
console.log("Perímetro:", a.perimeter());
console.log("Área:", a.area());
console.log();
a.draw();

console.log("Segundo rectángulo");
console.log("Ancho:", b.width);
console.log("Altura:", b.height);
console.log("Perímetro:", b.perimeter());
console.log("Área:", b.area());
console.log();
b.draw();
