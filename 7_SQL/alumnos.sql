create table alumnos (
    matricula   int,
    nombre      text,
    promedio    float
);

insert into alumnos values (123, 'Juan', 71.2);
insert into alumnos values (199, 'María', 98.7);
insert into alumnos values (666, 'Damian', 66.6);
insert into alumnos values (201, 'Gloria', 85.2);
